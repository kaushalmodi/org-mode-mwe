#+hugo_base_dir: .

#+columns: %30ITEM %TODO %30EXPORT_HUGO_SECTION_FRAG %TAGS


* Section fragment concatenation        :fragment:concatenation:deep_nesting:
** sub1 section
:PROPERTIES:
:EXPORT_HUGO_SECTION_FRAG: sub1
:END:
*** Post in sub1
:PROPERTIES:
:EXPORT_FILE_NAME: hugo-section-frag-post-in-posts-sub1
:END:
#+begin_description
Test post created in ~posts/sub1~.
#+end_description
*** sub1/sub2 section
:PROPERTIES:
:EXPORT_HUGO_SECTION_FRAG: sub2
:END:
**** Post in sub1/sub2
:PROPERTIES:
:EXPORT_FILE_NAME: hugo-section-frag-post-in-posts-sub1-sub2
:END:
#+begin_description
Test post created in ~posts/sub1/sub2~.
#+end_description
**** sub1/sub2/sub3 section
:PROPERTIES:
:EXPORT_HUGO_SECTION_FRAG: sub3
:END:
***** Post in sub1/sub2/sub3
:PROPERTIES:
:EXPORT_FILE_NAME: hugo-section-frag-post-in-posts-sub1-sub2-sub3
:END:
#+begin_description
Test post created in ~posts/sub1/sub2/sub3~.
#+end_description
** Post with all HUGO_SECTION, HUGO_SECTION_FRAG, EXPORT_FILE_NAME in same subtree
:PROPERTIES:
:EXPORT_HUGO_SECTION: articles
:EXPORT_HUGO_SECTION_FRAG: emacs
:EXPORT_FILE_NAME: hugo-SECTION_FRAG-frag-post-in-articles-emacs
:END:
#+begin_description
Test post created in ~articles/emacs~.
#+end_description
