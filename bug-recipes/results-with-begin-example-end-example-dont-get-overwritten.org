#+TITLE: Results with #+begin_example/#+end_example do not get overwritten
#+PROPERTY: header-args:python :exports both :results output

#+BEGIN_SRC python
print('a'.islower())
print('A'.islower())
print('abc'.islower())
print('Abc'.islower())
print('aBc'.islower())
print('012'.islower())
print('{}'.islower())
print('ABC'.islower())
print('À'.islower())
print('à'.islower())
#+END_SRC

#+RESULTS:
#+begin_example
True
False
True
False
False
False
False
False
False
True
#+end_example
